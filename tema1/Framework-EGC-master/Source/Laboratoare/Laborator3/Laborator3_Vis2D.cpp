#include "Laborator3_Vis2D.h"

#include <vector>
#include <iostream>
#include <math.h>
#include <time.h>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;

Laborator3_Vis2D::Laborator3_Vis2D() {}
Laborator3_Vis2D::~Laborator3_Vis2D() {}

void Laborator3_Vis2D::Init()
{
	srand(time(NULL));

	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	cameraInput->SetActive(false);

	logicSpace.x = 0;		// logic x
	logicSpace.y = 0;		// logic y
	logicSpace.width = 4;	// logic width
	logicSpace.height = 4;	// logic height

	//	Place spaceship on the middle of the screen
	spaceship.x = 2;
	spaceship.y = 2;
	
	glm::vec3 corner = glm::vec3(0, 0, 0);

	length = 0.2f;

	//	Initialize all the shapes
	Mesh* spaceshipObject = Object2D::CreateSpaceship("spaceshipObject", corner, length, glm::vec3(0.25, 0.9, 0.6));
	AddMeshToList(spaceshipObject);

	Mesh* lifeObject = Object2D::CreateRectangle("lifeObject", corner, 0.05f, 0.2f, glm::vec3(1, 1, 0.6));
	AddMeshToList(lifeObject);

	Mesh* missileObject = Object2D::CreateRectangle("missileObject", corner, 0.02f, 0.08f, glm::vec3(1, 1, 0.6));
	AddMeshToList(missileObject);

	Mesh* enemy1 = Object2D::CreateSpaceship("enemy1", corner, length, glm::vec3(0.36, 0.5, 0.9));
	AddMeshToList(enemy1);

	Mesh* enemy2 = Object2D::CreateSpaceship("enemy2", corner, length, glm::vec3(0.8, 0.8, 0.25));
	AddMeshToList(enemy2);

	Mesh* enemy2Red = Object2D::CreateSpaceship("enemy2Red", corner, length, glm::vec3(1, 0, 0));
	AddMeshToList(enemy2Red);
}

// 2D visualization matrix
glm::mat3 Laborator3_Vis2D::VisualizationTransf2D(const LogicSpace & logicSpace, const ViewportSpace & viewSpace)
{
	float sx, sy, tx, ty;
	sx = viewSpace.width / logicSpace.width;
	sy = viewSpace.height / logicSpace.height;
	tx = viewSpace.x - sx * logicSpace.x;
	ty = viewSpace.y - sy * logicSpace.y;

	return glm::transpose(glm::mat3(
		sx, 0.0f, tx,
		0.0f, sy, ty,
		0.0f, 0.0f, 1.0f));
}

void Laborator3_Vis2D::SetViewportArea(const ViewportSpace & viewSpace, glm::vec3 colorColor, bool clear)
{
	glViewport(viewSpace.x, viewSpace.y, viewSpace.width, viewSpace.height);

	glEnable(GL_SCISSOR_TEST);
	glScissor(viewSpace.x, viewSpace.y, viewSpace.width, viewSpace.height);

	// Clears the color buffer (using the previously set color) and depth buffer
	glClearColor(colorColor.r, colorColor.g, colorColor.b, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_SCISSOR_TEST);

	camera->SetOrthographic((float)viewSpace.x, (float)(viewSpace.x + viewSpace.width), (float)viewSpace.y, (float)(viewSpace.y + viewSpace.height), 0.1f, 400);
	camera->Update();
}

void Laborator3_Vis2D::Update(float deltaTimeSeconds)
{
	glm::ivec2 resolution = window->GetResolution();

	// Sets the screen area where to draw - the left half of the window
	viewSpace = ViewportSpace(0, 0, resolution.x, resolution.y);

	if (spaceship.lives > 0) {
		SetViewportArea(viewSpace, glm::vec3(0), true);
	} else {
		//	While the 2s has not passed, add some more redness
		if (currentTime >= Engine::GetElapsedTime() - lostTimeElapse) {
			redness += 1 / lostTimeElapse;
			SetViewportArea(viewSpace, glm::vec3(redness, 0, 0), true);
		}
	}

	// Compute the 2D visualization matrix
	visMatrix = glm::mat3(1);
	visMatrix *= VisualizationTransf2D(logicSpace, viewSpace);
	modelMatrix *= Transform2D::Translate(logicSpace.x, logicSpace.y);
	modelMatrix *= Transform2D::Scale(logicSpace.width, logicSpace.height);

	DrawScene(visMatrix, deltaTimeSeconds);
}

void Laborator3_Vis2D::DrawScene(glm::mat3 visMatrix, float deltaTime)
{
	if (spaceship.lives > 0) {
		GenerateRandomEnemy();
	}

	RenderLives();
	RenderHeroSpaceship();
	RenderEnemies(deltaTime);
	RenderMissiles();		
}

/**
* Generates a random enemy, with random coordinates on the arc of the logicspace circle view
*/
void Laborator3_Vis2D::GenerateRandomEnemy()
{
	//	Check if 2s has been elapsed
	if (Engine::GetElapsedTime() < 2) return;
	//	Check if the timeout for creating a new enemy has been reached
	if (currentTime < Engine::GetElapsedTime() - enemyGenerateInterval) {
		currentTime = Engine::GetElapsedTime();

		//	Update interval only if is greater than 0.5s
		if (enemyGenerateInterval > 0.5) {
			enemyGenerateInterval -= 0.05;
		}

		//	Create the random enemy
		int enemyLivesNo = rand() % 2 + 1;
		Enemy enemy(enemyLivesNo);

		//	Sets the enemy coordinates and add it to the enemies vector
		setEnemyPosition(enemy);
	}
}

/**
* Sets random starting points on the main circle arc for the enemy and adds it to the enemies vector
*/
void Laborator3_Vis2D::setEnemyPosition(Enemy enemy)
{
	//	random number between 0.0 - 2.0
	float r = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (logicSpace.width / 2)));
	float angle = r * M_PI;

	enemy.x = 2 + cos(angle) * 2;
	enemy.y = 2 + sin(angle) * 2;

	enemy.speed = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 0.7f)) + 0.2f;

	enemies.push_back(enemy);
}

/**
*	Render the lives
*/
void Laborator3_Vis2D::RenderLives() {
	for (int i = 0; i < spaceship.lifeShapes.size(); i++) {
		modelMatrix = visMatrix * Transform2D::Translate(spaceship.lifeShapes[i].x, spaceship.lifeShapes[i].y);
		RenderMesh2D(meshes["lifeObject"], shaders["VertexColor"], modelMatrix);
	}
}

/**
*	Render the hero spaceship
*/
void Laborator3_Vis2D::RenderHeroSpaceship() {
	modelMatrix = visMatrix * Transform2D::Translate(spaceship.x, spaceship.y);
	modelMatrix *= Transform2D::Rotate((float)spaceship.angle);
	modelMatrix *= Transform2D::Translate(-(length / 2), -(length / 2));

	RenderMesh2D(meshes["spaceshipObject"], shaders["VertexColor"], modelMatrix);
}

/**
*	Operations for enemies rendering: check for collision with hero spaceship,
*		follow hero spacehip, animate scaling on life lost and render each enemy
*	@param [float] deltaTime
*/
void Laborator3_Vis2D::RenderEnemies(float deltaTime) {
	for (int i = 0; i < enemies.size() && !enemies.empty(); i++) {
		Enemy &enemy = enemies[i];
		float realSpaceshipY = spaceship.y;

		if (spaceship.lives > 0) {
			//	Check for collision with enemy and remove enemy spaceship
			bool collision = pow(spaceship.x - enemy.x, 2) + pow(spaceship.y - enemy.y, 2) <= pow(spaceship.width / 2 + enemy.width / 2, 2);
			if (collision) {
				spaceship.getHit();

				if (spaceship.lives == 0) {
					currentTime = Engine::GetElapsedTime();
				}

				enemies.erase(enemies.begin() + i);
				continue;
			}

			//	Spaceship follow
			float dx = spaceship.x - enemy.x;
			float dy = spaceship.y - enemy.y;
			enemy.angle = atan2(dy, dx);

			enemy.x += enemy.speed * deltaTime * cos(enemy.angle);
			enemy.y += enemy.speed * deltaTime * sin(enemy.angle);
		}

		//	Enemy scale animation
		if (enemy.isScaling) {
			if (enemy.startScaleTime > Engine::GetElapsedTime() - 0.25) {
				enemy.scale -= 1 / 27.0f;
			} else {
				enemy.isScaling = false;
			}
		}

		//	Render enemy
		modelMatrix = visMatrix * Transform2D::Translate(enemy.x, enemy.y);
		modelMatrix *= Transform2D::Rotate(enemy.angle);
		modelMatrix *= Transform2D::Scale(enemy.scale, enemy.scale);
		modelMatrix *= Transform2D::Translate(-enemy.width / 2, -enemy.width / 2);
		RenderMesh2D(meshes[enemy.type], shaders["VertexColor"], modelMatrix);
	}
}

/**
*	Renders the missiles and checks for collision with enemy.
*		Also responsible with changes on collisions: clear bullet, make enemy lose a life
*/
void Laborator3_Vis2D::RenderMissiles() {
	for (int i = 0; i < missiles.size() && !missiles.empty(); i++) {
		if (spaceship.lives > 0) {
			missiles[i].x += 0.1 * sin(missiles[i].angle);
			missiles[i].y += 0.1 * cos(missiles[i].angle);
		}

		modelMatrix = visMatrix * Transform2D::Translate(missiles[i].x, missiles[i].y);
		modelMatrix *= Transform2D::Rotate(missiles[i].angle);
		modelMatrix *= Transform2D::Translate(-(missiles[i].width / 2), -(missiles[i].height / 2));
		RenderMesh2D(meshes["missileObject"], shaders["VertexColor"], modelMatrix);

		MissileCollision(i);
	}
}

/**
 * Check for missile collision with enemy ship;
 * Erase missile and enemy ship on collision
 * @param	int index - the index of the missile
 */
bool Laborator3_Vis2D::MissileCollision(int index) {
	Missile &missile = missiles[index];
	for (int j = 0; j < enemies.size() && !enemies.empty(); j++) {
		Enemy &enemy = enemies[j];
		bool collision = pow(enemy.x - missile.x, 2) + pow(enemy.y - missile.y, 2) <= pow(enemy.width / 2 + missile.width / 2, 2);

		if (collision) {
			enemy.getHit();

			missiles.erase(missiles.begin() + index);

			if (enemy.lives == 0) {
				enemies.erase(enemies.begin() + j);
			}

			return true;
		}
	}

	//	Remove missile if is out of the logic space
	if (missile.x > logicSpace.width || missile.y > logicSpace.height ||
		missile.x < 0 || missile.y < 0) {
		missiles.erase(missiles.begin() + index);
	}

	return false;
}

void Laborator3_Vis2D::OnInputUpdate(float deltaTime, int mods)
{
	//	Move spaceship
	if (spaceship.lives > 0) {
		if (window->KeyHold(GLFW_KEY_W)) {
			spaceship.y += spaceship.speed * deltaTime;
		}
		if (window->KeyHold(GLFW_KEY_S)) {
			spaceship.y -= spaceship.speed * deltaTime;
		}
		if (window->KeyHold(GLFW_KEY_A)) {
			spaceship.x -= spaceship.speed * deltaTime;
		}
		if (window->KeyHold(GLFW_KEY_D)) {
			spaceship.x += spaceship.speed * deltaTime;
		}
	}
}

void Laborator3_Vis2D::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	if (spaceship.lives == 0) return;

	//	Calculate rotation for hero spaceship
	float sx = logicSpace.width / viewSpace.width,
	sy = logicSpace.width / viewSpace.height;

	float xp = mouseX * sx;
	float yp = mouseY * sy;

	float dx = xp - spaceship.x;
	float dy = logicSpace.width - yp - spaceship.y;

	spaceship.angle = atan2(dx, dy);
};

void Laborator3_Vis2D::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
	if (spaceship.lives == 0) return;

	//	On left click, create new Missile and add it to the missiles array
	if (IS_BIT_SET(button, GLFW_MOUSE_BUTTON_LEFT)) {
		Missile newMissile;
		newMissile.x = spaceship.x;
		newMissile.y = spaceship.y;
		newMissile.angle = spaceship.angle;

		missiles.push_back(newMissile);
	}
};
