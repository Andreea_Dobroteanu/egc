#pragma once

#include <iostream>
#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>
#include <vector>

class Laborator3_Vis2D : public SimpleScene
{
	public:
		struct ViewportSpace
		{
			ViewportSpace() : x(0), y(0), width(1), height(1) {}
			ViewportSpace(int x, int y, int width, int height)
				: x(x), y(y), width(width), height(width) {}
			int x;
			int y;
			int width;
			int height;
		};

		struct LogicSpace
		{
			LogicSpace() : x(0), y(0), width(1), height(1) {}
			LogicSpace(float x, float y, float width, float height)
				: x(x), y(y), width(width), height(width) {}
			float x;
			float y;
			float width;
			float height;
		};

		struct Shape {
			Shape() : x(0), y(0), width(0), height(0), angle(0) {}
			Shape(float x, float y, float width, float height, float angle)
				: x(x), y(y), width(width), height(height), angle(angle) {}
			float x, y, width, height, angle;
		};

		struct Life : Shape {
			Life() : Shape(0, 0, 0.05f, 0.2f, 0) {}
			Life(float x, float y) : Shape(x, y, 0.05f, 0.2f, 0) {}
		};

		struct Missile : Shape {
			Missile() : Shape(0, 0, 0.02f, 0.08f, 0) {}
		};

		struct Spaceship : Shape {
			Spaceship() : Shape(0, 0, 0.2f, 0.2f, 0), lives(0), speed(0) {}
			Spaceship(int lives, float speed) : Shape(0, 0, 0.2f, 0.2f, 0), lives(lives), speed(speed) {}
			int lives;
			float speed;
			void getHit() {
				--lives;
			};
		};

		struct HeroSpaceship : Spaceship {
			HeroSpaceship() : Spaceship(3, 1.5f) {
				for (int i = 1; i < 4; i++) {
					float x = 4 - (i * 0.1) - 0.1f;
					float y = 4 - 0.3f;

					Life life(x, y);

					lifeShapes.push_back(life);
				}
			}
			std::vector<Life> lifeShapes;
			void getHit() {
				Spaceship::getHit();
				lifeShapes.erase(lifeShapes.begin());
			}
		};

		struct Enemy : Spaceship {
			Enemy(int lives) : 
				Spaceship(lives, 0.1f), type("enemy" + std::to_string(lives)), scale(1.0f), isScaling(false) {}

			float scale;
			float startScaleTime;
			bool isScaling;
			std::string type;
			std::string afterHitAction;

			void getHit() {
				if (lives > 1) {
					startScaleTime = Engine::GetElapsedTime();
					isScaling = true;
					speed *= 2;
					type = "enemy2Red";
				}
				Spaceship::getHit();
			}
		};

	public:
		Laborator3_Vis2D();
		~Laborator3_Vis2D();

		void Init() override;

	private:
		void Update(float deltaTimeSeconds) override;

		void DrawScene(glm::mat3 visMatrix, float deltaTime);

		void RenderLives();
		void RenderHeroSpaceship();
		void RenderEnemies(float deltaTime);
		void RenderMissiles();

		void GenerateRandomEnemy();
		void setEnemyPosition(Enemy enemy);
		bool MissileCollision(int index);


		void OnInputUpdate(float deltaTime, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;

		// Sets the logic space and view space
		glm::mat3 VisualizationTransf2D(const LogicSpace &logicSpace, const ViewportSpace &viewSpace);
		glm::mat3 VisualizationTransf2DUnif(const LogicSpace &logicSpace, const ViewportSpace &viewSpace);

		void SetViewportArea(const ViewportSpace &viewSpace, glm::vec3 colorColor = glm::vec3(0), bool clear = true);

	protected:
		float length;
		ViewportSpace viewSpace;
		LogicSpace logicSpace;
		glm::mat3 modelMatrix, visMatrix;

		HeroSpaceship spaceship;
		std::vector<Missile> missiles;
		std::vector<Enemy> enemies;
		std::vector<Life> lives;

		float currentTime = 0;
		float enemyGenerateInterval = 2;
		float lostTimeElapse = 200;
		float redness = 0;
};
