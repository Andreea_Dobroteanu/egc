#include "Object2D.h"

#include <Core/Engine.h>

/**
*	Creates a reclangle
*	@param [string] name - name of the mesh
*	@param [glm::vec3] leftBottomCorner
*	@param [float] width - width of the rectangle
*	@param [float] height - height of the rectangle
*	@param [glm::vec3] color - color of the rectangle
*/
Mesh* Object2D::CreateRectangle(std::string name, glm::vec3 leftBottomCorner, float width, float height, glm::vec3 color)
{
	glm::vec3 corner = leftBottomCorner;
	
	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(width, 0, 0), color),
		VertexFormat(corner + glm::vec3(width, height, 0), color),
		VertexFormat(corner + glm::vec3(0, height, 0), color)
	};

	Mesh* rectangle = new Mesh(name);
	std::vector<unsigned short> indices = {
		0, 1, 2,
		0, 3, 2
	};

	rectangle->InitFromData(vertices, indices);

	return rectangle;
}

/**
*	Creates a spaceship
*	@param [string] name - name of the mesh
*	@param [glm::vec3] leftBottomCorner
*	@param [float] width
*	@param [float] height
*	@param [glm::vec3] color
*/
Mesh* Object2D::CreateSpaceship(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, length, 0), color),
		VertexFormat(corner + glm::vec3(0, length, 0), color),
		VertexFormat(corner + glm::vec3(length / 2, length / 2, 0), color)
	};

	Mesh* spaceship = new Mesh(name);
	std::vector<unsigned short> indices = {
		4, 1, 2,
		3, 4, 0
	};
	
	spaceship->InitFromData(vertices, indices);

	return spaceship;
}