#pragma once

#include <string>

#include <include/glm.h>
#include <Core/GPU/Mesh.h>

namespace Object2D
{
	Mesh* CreateRectangle(std::string name, glm::vec3 leftBottomCorner, float width, float height, glm::vec3 color);
	Mesh* CreateSpaceship(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color);
}

