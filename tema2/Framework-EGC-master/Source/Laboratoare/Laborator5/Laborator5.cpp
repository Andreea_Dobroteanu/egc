#include "Laborator5.h"

#include <vector>
#include <string>
#include <iostream>
#include <typeinfo>

#include <Core/Engine.h>

using namespace std;
float angle = 60;
float bot = -0.5f, up = 0.5f, r = 0.5f, l = -0.5f;

Laborator5::Laborator5()
{
}

Laborator5::~Laborator5()
{
}

void Laborator5::Init()
{
	renderCameraTarget = false;

	camera = new Laborator::Camera();
	camera->Set(glm::vec3(0, 2, 2), glm::vec3(0, 1, 0), glm::vec3(0, 1, 0));


	hero.angleY = RADIANS(180);
		
	{
		Mesh* mesh = new Mesh("c3po");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "C3PO.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("trex");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "trex.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("sphere");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "sphere.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	{
		Mesh* mesh = new Mesh("tower");
		mesh->LoadMesh(RESOURCE_PATH::MODELS + "Primitives", "tower.obj");
		meshes[mesh->GetMeshID()] = mesh;
	}

	InitTowers();

	projectionMatrix = glm::perspective(RADIANS(angle), window->props.aspectRatio, 0.01f, 200.0f);
}

void Laborator5::InitTowers()
{
	Tower bottomLeftTower(glm::vec3(0, 0, -2.5f));
	towers.push_back(bottomLeftTower);

	Tower bottomRightTower(glm::vec3(-2.5f, 0, 0));
	towers.push_back(bottomRightTower);

	Tower topLeftTower(glm::vec3(2.5f, 0, 0));
	towers.push_back(topLeftTower);

	Tower topRightTower(glm::vec3(0, 0, 2.5f));
	towers.push_back(topRightTower);
}

void Laborator5::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator5::Update(float deltaTimeSeconds)
{
	//	Hero
	{
		glm::mat4 modelMatrix = glm::mat4(1);
		modelMatrix = glm::translate(modelMatrix, glm::vec3(hero.x, hero.y, hero.z));
		modelMatrix = glm::rotate(modelMatrix, hero.angleX, glm::vec3(1, 0, 0));
		modelMatrix = glm::rotate(modelMatrix, hero.angleY, glm::vec3(0, 1, 0));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(0.01f));

		if (hero.isDead) {
			if (hero.angleX < RADIANS(90)) {
				hero.angleX += deltaTimeSeconds;
			}
			else {
				gameOver = true;
			}
		}

		if (activeWeapon != 2) {
			RenderMesh(meshes["c3po"], shaders["Color"], modelMatrix);
		}

	}

	{
		GenerateNewEnemy();
		for (int i = 0; i < enemies.size(); i++) {
			Enemy& enemy = enemies[i];
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(enemy.x, enemy.y, enemy.z));
			modelMatrix = glm::rotate(modelMatrix, enemy.angleX, glm::vec3(1, 0, 0));
			modelMatrix = glm::rotate(modelMatrix, enemy.angleY, glm::vec3(0, 1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(enemy.scale));
			RenderMesh(meshes["trex"], shaders["VertexNormal"], modelMatrix);

			if (enemy.isDead) {
				DeadEnemyEffect(i, deltaTimeSeconds);
				continue;
			} else {
				if (!gameOver) {
					MoveEnemy(i, deltaTimeSeconds);
				}
			}
		}
	}

	{
		for (int i = 0; i < weaponMissiles.size(); i++) {
			Weapon &weapon = weaponMissiles[i];
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(weapon.coords.x, weapon.coords.y, weapon.coords.z));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));

			RenderMesh(meshes["sphere"], shaders["VertexNormal"], modelMatrix);

			if (gameOver) continue;

			weapon.coords.x += deltaTimeSeconds * weapon.speed * sin(weapon.angle);
			weapon.coords.z += deltaTimeSeconds * weapon.speed * cos(weapon.angle);

			if (weapon.type == 3) {
				if (weapon.coords.y > 1.5f) {
					weapon.coords.x += deltaTimeSeconds * weapon.speed * sin(weapon.angle);
					weapon.coords.z += deltaTimeSeconds * weapon.speed * cos(weapon.angle);
					weapon.yDir = -1;
				}

				weapon.coords.y += weapon.yDir * deltaTimeSeconds * weapon.speed * cos(RADIANS(20));
			}


			if (abs(weapon.initCoords.z - weapon.coords.z) > weapon.distance || 
				abs(weapon.initCoords.x - weapon.coords.x) > weapon.distance) {
				weaponMissiles.erase(weaponMissiles.begin() + i);
			} else {
				CheckEnemyColision(i, deltaTimeSeconds);
			}

		}
	}

	//	Towers
	{
		for (int i = 0; i < towers.size(); i++) {
			Tower &tower = towers[i];
			glm::mat4 modelMatrix = glm::mat4(1);
			modelMatrix = glm::translate(modelMatrix, glm::vec3(tower.coords.x, tower.coords.y, tower.coords.z));
			modelMatrix = glm::rotate(modelMatrix, tower.angle, glm::vec3(0, 1, 0));
			modelMatrix = glm::scale(modelMatrix, glm::vec3(0.003f));

			RenderMesh(meshes["tower"], shaders["VertexNormal"], modelMatrix);

			if (gameOver) continue;

			if (Engine::GetElapsedTime() - tower.lastShootTime > 1.0f) {
				tower.lastShootTime = Engine::GetElapsedTime();
				for (int j = 0; j < enemies.size(); j++) {
					Enemy& enemy = enemies[j];
					bool enemyInRange = pow(tower.coords.x - enemy.x, 2) + pow(tower.coords.z - enemy.z, 2) <= 20;

					if (enemyInRange) {

						TowerMissile missile(glm::vec3(tower.coords.x, 2, tower.coords.z));
						missile.enemyX = enemy.x;
						missile.enemyZ = enemy.z;

						tower.missiles.push_back(missile);
					}
				}
			}

			for (int j = 0; j < tower.missiles.size(); j++) {
				TowerMissile &missile = tower.missiles[j];
				glm::mat4 modelMatrix = glm::mat4(1);

				modelMatrix = glm::translate(modelMatrix, glm::vec3(missile.coords.x, missile.coords.y, missile.coords.z));
				modelMatrix = glm::scale(modelMatrix, glm::vec3(0.2f));

				float dist = sqrt(powf(tower.coords.x - missile.enemyX, 2) + powf(tower.coords.x - missile.enemyZ, 2));
				float missileSin = (-tower.coords.x + missile.enemyX) / dist;
				float missileCos = (-tower.coords.z + missile.enemyZ) / dist;

				missile.coords.x += missile.speed * deltaTimeSeconds * missileSin;
				missile.coords.z += missile.speed * deltaTimeSeconds * missileCos;
				missile.coords.y -= deltaTimeSeconds * missile.speed * sin(RADIANS(30));

				RenderMesh(meshes["sphere"], shaders["VertexNormal"], modelMatrix);

				for (int i = 0; i < enemies.size(); i++) {
					Enemy &enemy = enemies[i];

					bool collision = pow(missile.coords.x - enemy.x, 2) + pow(missile.coords.z - enemy.z, 2) <= pow(0.8, 2);

					if (collision) {

						enemy.lives -= missile.power;

						if (enemy.lives <= 0) {
							enemy.isDead = true;
						}

						tower.missiles.erase(tower.missiles.begin() + j);
						return;
					}
				}

				if (missile.coords.y < 0) {
					tower.missiles.erase(tower.missiles.begin() + j);
				}
			}
		}
	}
}


void Laborator5::CheckEnemyColision(int weaponIndex, float deltaTime)
{
	Weapon &weapon = weaponMissiles[weaponIndex];

	for (int i = 0; i < enemies.size(); i++) {
		Enemy& enemy = enemies[i];

		bool collision = pow(weapon.coords.x - enemy.x, 2) + pow(weapon.coords.z - enemy.z, 2) <= pow(0.8, 2);

		if (collision) {

			enemy.lives -= weapon.power;

			if (enemy.lives <= 0) {
				enemy.isDead = true;
			}

			weaponMissiles.erase(weaponMissiles.begin() + weaponIndex);
			return;
		}
	}
}

void Laborator5::FrameEnd()
{
	DrawCoordinatSystem(camera->GetViewMatrix(), projectionMatrix);
}

void Laborator5::RenderMesh(Mesh * mesh, Shader * shader, const glm::mat4 & modelMatrix)
{
	if (!mesh || !shader)
		return;

	// render an object using the specified shader and the specified position
	shader->Use();
	glUniformMatrix4fv(shader->loc_view_matrix, 1, false, glm::value_ptr(camera->GetViewMatrix()));
	glUniformMatrix4fv(shader->loc_projection_matrix, 1, false, glm::value_ptr(projectionMatrix));
	glUniformMatrix4fv(shader->loc_model_matrix, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	mesh->Render();
}

void Laborator5::OnInputUpdate(float deltaTime, int mods)
{
	if (window->KeyHold(GLFW_KEY_1)) {
		activeWeapon = 1;
		projectionMatrix = glm::perspective(RADIANS(angle), window->props.aspectRatio, 0.01f, 200.0f);
	}

	if (window->KeyHold(GLFW_KEY_2)) {
		activeWeapon = 2;
		projectionMatrix = glm::perspective(RADIANS(20), window->props.aspectRatio, 0.01f, 200.0f);
	}

	if (window->KeyHold(GLFW_KEY_3)) {
		activeWeapon = 3;
		projectionMatrix = glm::perspective(RADIANS(angle), window->props.aspectRatio, 0.01f, 200.0f);
	}

	if (window->KeyHold(GLFW_KEY_H)) {
		angle += deltaTime;
		projectionMatrix = glm::perspective(angle, window->props.aspectRatio, 0.01f, 200.0f);
	}

	if (window->KeyHold(GLFW_KEY_J)) {
		angle -= deltaTime;
		projectionMatrix = glm::perspective(angle, window->props.aspectRatio, 0.01f, 200.0f);
	}

	if (window->KeyHold(GLFW_KEY_K)) {
		bot -= deltaTime * 10;
		up += deltaTime * 10;
		projectionMatrix = glm::ortho(l, r, bot, up, 0.01f, 20.0f);
	}

	if (window->KeyHold(GLFW_KEY_L)) {
		l -= deltaTime * 10;
		r += deltaTime * 10;
		projectionMatrix = glm::ortho(l, r, bot, up, 0.01f, 20.0f);
	}

	// move the camera only if MOUSE_RIGHT button is pressed
	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float cameraSpeed = 2.0f;

		if (window->KeyHold(GLFW_KEY_W)) {
			camera->MoveForward(deltaTime * cameraSpeed);
			hero.x += deltaTime * cameraSpeed * sin(hero.angleY);
			hero.z += deltaTime * cameraSpeed * cos(hero.angleY);
		}

		if (window->KeyHold(GLFW_KEY_A)) {
			camera->TranslateRight(-deltaTime * cameraSpeed);
			hero.z -= deltaTime * cameraSpeed * sin(hero.angleY);
			hero.x += deltaTime * cameraSpeed * cos(hero.angleY);
		}

		if (window->KeyHold(GLFW_KEY_S)) {
			camera->MoveForward(-deltaTime * cameraSpeed);
			hero.x -= deltaTime * cameraSpeed * sin(hero.angleY);
			hero.z -= deltaTime * cameraSpeed * cos(hero.angleY);
		}

		if (window->KeyHold(GLFW_KEY_D)) {
			camera->TranslateRight(deltaTime * cameraSpeed);
			hero.z += deltaTime * cameraSpeed * sin(hero.angleY);
			hero.x -= deltaTime * cameraSpeed * cos(hero.angleY);
		}

		if (window->KeyHold(GLFW_KEY_Q)) {
			camera->TranslateUpword(-deltaTime * cameraSpeed);
		}

		if (window->KeyHold(GLFW_KEY_E)) {
			// TODO : translate the camera up
			camera->TranslateUpword(deltaTime * cameraSpeed);
		}
	}
}

void Laborator5::OnKeyPress(int key, int mods)
{
	// add key press event
	if (key == GLFW_KEY_T)
	{
		renderCameraTarget = !renderCameraTarget;
	}

	if (key == GLFW_KEY_O)
	{
		projectionMatrix = glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 10.0f, -25.0f);
	}

	if (key == GLFW_KEY_P)
	{
		projectionMatrix = glm::perspective(RADIANS(60), window->props.aspectRatio, 0.01f, 200.0f);
	}
}

void Laborator5::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator5::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event

	if (window->MouseHold(GLFW_MOUSE_BUTTON_RIGHT))
	{
		float sensivityOX = 0.001f;
		float sensivityOY = 0.001f;

		camera->RotateThirdPerson_OY(deltaX * sensivityOX);
		camera->RotateThirdPerson_OX(deltaY * sensivityOY);

		hero.angleY += deltaX * sensivityOX;
	}
}

void Laborator5::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button press event
	if (IS_BIT_SET(button, GLFW_MOUSE_BUTTON_LEFT)) {
		if (activeWeapon == 1) {
			if ((Engine::GetElapsedTime() - lastShootTime < pistolShootTimeout) && !weaponMissiles.empty()) return;

			Pistol pistol(glm::vec3(hero.x, 0, hero.z), hero.angleY);
			weaponMissiles.push_back(pistol);

			lastShootTime = Engine::GetElapsedTime();
		} else if (activeWeapon == 2) {
			if ((Engine::GetElapsedTime() - lastShootTime > riffleShootTimeout) && !weaponMissiles.empty()) return;

			Riffle riffle(glm::vec3(hero.x, 0, hero.z), hero.angleY);
			weaponMissiles.push_back(riffle);

			lastShootTime = Engine::GetElapsedTime();
		} else if (activeWeapon == 3) {
			if ((Engine::GetElapsedTime() - lastShootTime > pistolShootTimeout) && !weaponMissiles.empty()) return;

			Grenade grenade(glm::vec3(hero.x, 0, hero.z), hero.angleY);
			weaponMissiles.push_back(grenade);

			lastShootTime = Engine::GetElapsedTime();
		}
	}
}

void Laborator5::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator5::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator5::OnWindowResize(int width, int height)
{
}

void Laborator5::GenerateNewEnemy()
{
	if (Engine::GetElapsedTime() - savedIncTime > incEnemyLifeTTL) {
		savedIncTime = Engine::GetElapsedTime();
		++enemyLivesNo;
	}

	if (savedTime < Engine::GetElapsedTime() - enemyGenInterval) {
		savedTime = Engine::GetElapsedTime();

		Enemy enemy(enemyLivesNo);
		enemies.push_back(enemy);
	}
}

void Laborator5::MoveEnemy(int enemyIndex, float deltaTime) 
{
	Enemy& enemy = enemies[enemyIndex];
	if (enemy.x < 20 && enemy.z == -20) {
		enemy.x += deltaTime * enemy.speed;
		return;
	}

	//	Enemy reaches bottom right corner
	if (enemy.x >= 20 && enemy.z == -20) {
		//	Rotate enemy
		enemy.angleY = RADIANS(-45);
		enemy.z += deltaTime * enemy.speed;
		enemy.x -= deltaTime * enemy.speed;
		return;
	}

	// ZIG
	if (enemy.z > -20 && enemy.z < 20 && enemy.x < 20) {
		enemy.z += deltaTime * enemy.speed;
		enemy.x -= deltaTime * enemy.speed;
		return;
	}

	//	Enemy reaches top left corner
	if (enemy.x <= -20 && enemy.z >= 20) {
		enemy.angleY = RADIANS(90);
		enemy.x += deltaTime * enemy.speed;
		return;
	}

	if (enemy.z >= 20 && enemy.x >= -20 && enemy.x <= 20) {
		enemy.x += deltaTime * enemy.speed;
		return;
	}

	//	Enemy reaches top right corner; should dissapear
	if (enemy.x >= 20 && enemy.z >= 20) {
		DeadEnemyEffect(enemyIndex, deltaTime);

		if (enemy.reachedEnd) {
			--hero.lives;
			if (hero.lives <= 0) {
				hero.isDead = true;
			}
		}
	}
}

void Laborator5::DeadEnemyEffect(int enemyIndex, float deltaTime)
{
	Enemy &enemy = enemies[enemyIndex];

	if (enemy.reachedEnd) {
		enemies.erase(enemies.begin() + enemyIndex);
	}

	if (enemy.angleX < RADIANS(90)) {
		enemy.angleX += deltaTime;
	} else {
		enemy.reachedEnd = true;
	}
}

