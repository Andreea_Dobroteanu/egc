#pragma once
#include <Component/SimpleScene.h>
#include "LabCamera.h"

class Laborator5 : public SimpleScene
{
	public:
		Laborator5();
		~Laborator5();

		struct Weapon {
			Weapon() : initCoords(0, 0, 0), coords(0, 0, 0), distance(0), speed(0), timeout(0), power(0), angle(0), yDir(1){}
			Weapon(glm::vec3 coords, float distance, float speed, float timeout, float power, float angle, int type) : coords(coords), initCoords(coords),
				distance(distance), speed(speed), timeout(timeout), power(power), angle(angle), type(type), yDir(1) {}
			glm::vec3 initCoords, coords;
			float distance, speed, timeout, shootTime, power, angle;
			int type, yDir;
		};

		struct Pistol : Weapon {
			Pistol() : Weapon(glm::vec3(0, 0, 0), 10, 10, 0.5f, 2, 0, 1) {}
			Pistol(glm::vec3 coords, float angle) : Weapon(glm::vec3(coords), 10, 10, 1, 2, angle, 1) {}
			static float shootTimeout;
		};

		struct Riffle : Weapon {
			Riffle() : Weapon(glm::vec3(0, 0, 0), 30, 15, 0.2f, 3, 0, 2) {}
			Riffle(glm::vec3 coords, float angle) : Weapon(glm::vec3(coords), 30, 15, 0.5f, 1, angle, 2) {}
			static float shootTimeout;
		};

		struct Grenade : Weapon {
			Grenade() : Weapon(glm::vec3(0, 0, 0), 3, 2, 1, 3, 0, 3) {}
			Grenade(glm::vec3 coords, float angleX) : Weapon(glm::vec3(coords), 3, 2, 1.0f, 3, angleX, 3), angleY(0) {}
			float angleY;
		};

		struct Character {
			Character() : x(0), y(0), z(0), scale(1), angleX(0), angleY(0), speed(2), lives(3), isDead(false) {}
			Character(float x, float y, float z, float scale, float angleX, float angleY, float speed, int lives)
				: x(x), y(y), z(z), scale(scale), angleX(angleX), angleY(angleY), speed(speed), lives(lives), isDead(false) {}
			float x, y, z, scale, angleX, angleY, speed;
			int lives;
			bool isDead;
		};

		struct Hero : Character {
			Hero() : Character(0, 0, 0, 0.15f, 0, 180, 5, 3) {}
		};

		struct Enemy : Character {
			Enemy(int livesNo) : Character(-20.0f, 0, -20.0f, 0.4f, 0, 90, 3, livesNo), reachedEnd(false) {}
			bool reachedEnd;
		};

		struct TowerMissile : Weapon {
			TowerMissile(glm::vec3 coords) : Weapon(glm::vec3(coords), 3, 3, 1, 3, 0, 4) {}
			float enemyX, enemyZ;
		};

		struct Tower {
			Tower() : coords(glm::vec3(0, 0, 0)), angle(0), range(4), lastShootTime(0) {}
			Tower(glm::vec3 coords) : coords(glm::vec3(coords)), angle(0), range(4), lastShootTime(0) {}
			glm::vec3 coords;
			float angle, range, lastShootTime;
			std::vector<TowerMissile> missiles;
		};

		void Init() override;
		void InitTowers();
		void GenerateNewEnemy();
		void MoveEnemy(int enemyIndex, float deltaTime);
		void CheckEnemyColision(int weaponIndex, float deltaTime);
		void DeadEnemyEffect(int enemyIndex, float deltaTime);
		void TowerShootEnemy(int towerIndex, int missileIndex, float deltaTime);

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void RenderMesh(Mesh * mesh, Shader * shader, const glm::mat4 & modelMatrix) override;

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

	protected:
		Laborator::Camera *camera;
		glm::mat4 projectionMatrix;
		bool renderCameraTarget;
		int fov;
		float var;
		int enemyLivesNo = 1, 
			incEnemyLifeTTL = 20, 
			savedIncTime = 0, 
			enemyGenInterval = 5, 
			savedTime = 0, 
			activeWeapon = 1;

		float cameraZ = 3.5f,
			lastShootTime = 0, 
			pistolShootTimeout = 1,
			riffleShootTimeout = 0.5f;

		Hero hero;
		std::vector<Tower> towers;
		std::vector<Enemy> enemies;
		std::vector<Weapon> weaponMissiles;

		bool gameOver = false;
};
